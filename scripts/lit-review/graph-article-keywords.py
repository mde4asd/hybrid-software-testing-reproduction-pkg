"""
Script used to graph data about the keywords of articles.
Can be used to graph Author or Index keywords.
Can draw bubble, line and bar charts.
"""

import re
from collections import defaultdict
from enum import Enum
from typing import Union, List, Set

import matplotlib
import pandas as pd
import matplotlib.pyplot as plt
from nltk.stem.porter import PorterStemmer

# Columns in the CSV dataset:
# Authors,Author(s) ID,Title,Year,Source title,Volume,Issue,Art. No.,Page start,Page end,Page count,Cited by,DOI,Link,Affiliations,Authors with affiliations,Abstract,Author Keywords,Index Keywords,Molecular Sequence Numbers,Chemicals/CAS,Tradenames,Manufacturers,Funding Details,Funding Text 1,Funding Text 2,Funding Text 3,References,Correspondence Address,Editors,Sponsors,Publisher,Conference name,Conference date,Conference location,Conference code,ISSN,ISBN,CODEN,PubMed ID,Language of Original Document,Abbreviated Source Title,Document Type,Publication Stage,Open Access,Source,EID

GRAPH_DIR = "graphs"  # directory to save outputted images

STEMMED_KEYWORD_MAP = {  # maps word stems generated during keyword consolidation to their full-text counterparts
    # Top index keywords
    "accept test": "acceptance testing",
    "test driven develop": "test driven development",
    "softwar test": "software testing",
    "autom": "automation",
    "test": "testing",
    "unit test": "unit testing",
    "comput softwar select and evalu": "computer software selection and evaluation",
    "softwar engin": "software engineering",
    "open sourc softwar": "open source software",
    "softwar design": "software design",
    # Top author kws
    "mutat test": "mutation testing",
    "model base test": "model based testing",
    "test data gener": "test data generation",
    "genet algorithm": "genetic algorithms",
    "integr test": "integration testing",
    "autom test": "automated testing",
    "test gener": "test generation",
    "test case gener": "test case generation",
    "properti base test": "property based testing",
    "test autom": "test automation",
    "symbol execut": "symbolic execution",
    "autom test gener": "automatic test generation",
    # other keywords of interest
    "search base softwar test": "search based testing",
    "search base": "search based testing",
    "search base test": "search based testing",
    "unit test gener": "unit test generation",
    "automat test gener": "automatic test generation",
    "automat test case gener": "automatic test generation",
    "visual gui test": "visual gui testing",
    "parameter unit test": "parameterised unit testing",
    "test driven develop process": "test driven development",
    "behavior driven develop": "behaviour driven development",
    "accept test driven develop": "test driven development",
    "model base test approach": "model based testing",
    "behavior driven development(bdd)": "behaviour driven development",

    # "automat test pattern gener": "automated test pattern generation",  # this KW is related to electronics

}


class KeywordType(Enum):
    # enum values are the Scopus export column names
    Index = "Index Keywords"
    Author = "Author Keywords"


class GraphType(Enum):
    # enum values are names used for image export
    Bubble = "bubble"
    Line = "line"
    Bar = "bar"


def graph_keywords(scopus: pd.DataFrame, keyword_type: KeywordType, graph_type: GraphType,
                   keywords_to_graph: Union[List[str], Set[str], int]):
    """
    graphs the keywords of each article in the secondary pool.
    :param scopus: the data about the papers to include in the graph
    :param keyword_type: the type of keyword to graph
    :param graph_type: the type of graph to use. Bubble and Line shows count by year for each kw, Bar shows count per kw
    :param keywords_to_graph: a list of the keywords to graph. If an int x is provided, the top x keywords are graphed.
    """
    keyword_type = keyword_type.value  # convert to column name

    # key is the keyword, values are a dictionary of year -> count values.
    keyword_count_by_year = defaultdict(lambda: defaultdict(int))
    for i, article in scopus.iterrows():
        keywords_string = article[keyword_type]
        year = article['Year']
        keywords_of_article = consolidate_article_keywords(keywords_string)

        for keyword in keywords_of_article:
            keyword_count_by_year[keyword][year] += 1

    keyword_occurances = {}
    for kw, count_by_year in keyword_count_by_year.items():
        keyword_occurances[kw] = sum(count_by_year.values())

    print("all occurance counts:")
    for kw, occurances in sorted(keyword_occurances.items(), key=lambda x: -x[1]):
        if occurances == 1:
            print(f"{kw}: {occurances} papers")

    if isinstance(keywords_to_graph, int):
        top_keywords = sorted(keyword_occurances.items(), key=lambda x: -x[1])[:keywords_to_graph]
        keywords_to_graph = [kw for kw, _ in top_keywords]

    if graph_type == GraphType.Bubble:
        kw_year_counts_list = []
        for kw, count_by_year in keyword_count_by_year.items():
            if kw not in keywords_to_graph:
                continue
            for year, count in count_by_year.items():
                kw_year_counts_list.append((kw, year, count))

        bubble_size = 15
        # don't show 2021, since few papers appear for this year so it throws off the trend
        kw_year_counts_list = [(kw, year, count) for kw, year, count in kw_year_counts_list if year != 2021]
        kw_year_counts_list.sort(key=lambda x: x[0], reverse=True)
        kw_year_counts_list.sort(key=lambda x: keyword_occurances[x[0]])

        df = pd.DataFrame(kw_year_counts_list, columns=['keyword', 'year', 'count'])
        df.plot.scatter(x="year", y="keyword", s=df["count"] * bubble_size)
        plt.xlabel("Year")

    elif graph_type == GraphType.Line:
        data_to_graph = {kw: keyword_count_by_year[kw] for kw in keyword_count_by_year if kw in keywords_to_graph}
        df = pd.DataFrame(data_to_graph, index=list(range(2013, 2021)))
        df.plot.line()
        plt.xlabel("Year")

    elif graph_type == GraphType.Bar:
        data_to_graph = {kw: keyword_occurances[kw] for kw in keyword_occurances if kw in keywords_to_graph}
        data_to_graph = sorted(data_to_graph.items(), key=lambda x: x[1])
        keywords, occurances = zip(*[(kw, count) for kw, count in data_to_graph])
        df = pd.DataFrame({'keyword': keywords, 'count': occurances})
        df.plot.barh(x='keyword', y='count', legend=False)
        plt.xlabel("Count")

    plt.ylabel(keyword_type[:-1].capitalize())
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(9, 4.4)
    plt.tight_layout()
    plt.savefig(
        f"{GRAPH_DIR}/secondary-pool-popular-{'-'.join((keyword_type.split(' '))).lower()}-{graph_type.value}.png",
        dpi=600)
    plt.show()


def consolidate_article_keywords(semicolon_separated_keywords: str):
    """takes a semicolon separated string of keywords and returns a list of the keywords in the article
    uses word stemming to consolidate similar keywords (e.g. 'unit tests' and 'unit testing')"""
    keywords_of_article = pd.Series([keyword.strip() for keyword in semicolon_separated_keywords.split(';')])
    keywords_of_article = keywords_of_article.map(lambda k: k.lower()) \
        .map(lambda k: k.replace('-', ' ')) \
        .map(lambda k: "test driven development" if k == "tdd" else k) \
        .map(lambda k: "behaviour driven development" if k == "bdd" else k) \
        .map(lambda k: "acceptance test driven development" if k == "atdd" else k)
    keywords_of_article = keywords_of_article.map(gen_keyword_stemmer({r'\(.*\)'}))
    keywords_of_article = keywords_of_article.map(lambda kw: STEMMED_KEYWORD_MAP.get(kw, kw))
    keywords_of_article.drop_duplicates()
    return keywords_of_article.unique()


def gen_keyword_stemmer(word_exclusions=None, stem_exclusions=None):
    if word_exclusions is None:
        word_exclusions = set()
    if stem_exclusions is None:
        stem_exclusions = set()

    def keyword_stemmer(keyword):
        """reduces a keyword into its lexicographical word stems
        e.g., the stem of the keyword 'acceptance testing' is 'accept test'
        this is used to consolidate similar keywords such as 'unit testing' and 'unit tests'."""
        words = [word for word in keyword.split()]
        words = [word for word in words if not any(re.match(excl, word) for excl in word_exclusions)]
        stems = [PorterStemmer().stem(word) for word in words]
        stems = [stem for stem in stems if stem not in stem_exclusions]
        return ' '.join(stems)

    return keyword_stemmer


def count_unit_and_acceptance_uses(scopus: pd.DataFrame, keyword_type: KeywordType):
    """counts the number of times the unit and acceptance testing keywords appear in the pool.
    shows counts for papers that use only unit, only acceptance, both keywords an neither keyword."""
    keyword_type = keyword_type.value

    unit = 0
    acc = 0
    both = 0
    neither = 0
    for i, article in scopus.iterrows():
        keywords_string = article[keyword_type]
        year = article['Year']
        keywords_of_article = consolidate_article_keywords(keywords_string)

        ut = 'unit testing'
        at = 'acceptance testing'
        if ut in keywords_of_article:
            unit += 1
        if at in keywords_of_article:
            acc += 1
            # print(*scopus[scopus[keyword_type] == keywords_string]['Author Keywords'])
        if len({ut, at}.intersection(set(keywords_of_article))) == 2:
            both += 1
        if len({ut, at}.intersection(set(keywords_of_article))) == 0:
            neither += 1

    print(f"{unit} with unit testing keyword")
    print(f"{acc} with acceptance testing keyword")
    print(f"{both} with both keywords")
    print(f"{neither} with neither keyword")


def main():
    font = {'family': 'normal',
            'size': 16}
    matplotlib.rc('font', **font)

    for keyword_type in [KeywordType.Index, KeywordType.Author]:
        scopus = pd.read_csv('scopus-joined.csv')
        scopus = scopus.drop(scopus[~scopus["yes/no/maybe"].str.contains("y")].index)
        scopus = scopus.dropna(subset=[keyword_type.value])  # removes null values to avoid graphing errors

        # interesting_kws = ["software testing", "software engineering", "unit testing", "acceptance testing",
        #                    "test driven development", "model based testing", "test generation", "test case generation",
        #                    "search based testing", ]
        strategies_of_interest = {"test driven development",
                                  "model based testing",
                                  "test generation",
                                  "test case generation",
                                  "search based testing",
                                  "unit test generation",
                                  "automatic test generation",
                                  "automatic test case generation",
                                  "unit test generation",
                                  "automatic test generation",
                                  "visual gui testing",
                                  "parameterised unit testing",
                                  "test driven development",
                                  "behaviour driven development"}

        print(f"U/A KEYWORD USES for {keyword_type.value}:")
        count_unit_and_acceptance_uses(scopus, keyword_type)
        print()
        print("generating Bubble chart of top 6 entries...")
        graph_keywords(scopus, keyword_type, GraphType.Bubble, 6)
        print()
        print("generating Bar chart of popular strategies...")
        graph_keywords(scopus, keyword_type, GraphType.Bar, strategies_of_interest)


if __name__ == '__main__':
    main()
