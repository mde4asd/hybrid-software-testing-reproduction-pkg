"""
tutorial: https://towardsdatascience.com/topic-modelling-in-python-with-nltk-and-gensim-4ef03213cd21
possible citation: https://journalofbigdata.springeropen.com/articles/10.1186/s40537-019-0255-7
"""

import spacy
import nltk
import random
import pandas as pd
from spacy.lang.en import English
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer
import gensim
import pickle

spacy.load("en_core_web_sm")
parser = English()
nltk.download('wordnet')
nltk.download('stopwords')
en_stop = set(nltk.corpus.stopwords.words('english'))
# en_stop = en_stop.union({
#     "test", "testing", "language", "strategy", "paper", "approach"
# })

NUM_TOPICS = 4


def tokenize(text):
    lda_tokens = []
    tokens = parser(text)
    for token in tokens:
        if token.orth_.isspace():
            continue
        elif token.like_url:
            lda_tokens.append('URL')
        elif token.orth_.startswith('@'):
            lda_tokens.append('SCREEN_NAME')
        else:
            lda_tokens.append(token.lower_)
    return lda_tokens


def get_lemma(word):
    return WordNetLemmatizer().lemmatize(word)
    # lemma = wn.morphy(word)
    # if lemma is None:
    #     return word
    # else:
    #     return lemma


def prepare_text_for_lda(text):
    tokens = tokenize(text)
    tokens = [token for token in tokens if len(token) > 4]
    tokens = [token for token in tokens if token not in en_stop]
    tokens = [get_lemma(token) for token in tokens]
    return tokens


def main():
    text_data = []
    with open('scopus-joined.csv') as f:
        scopus = pd.read_csv(f)
        # print(scopus['Abstract'])
        for line in scopus['Abstract']:
            tokens = prepare_text_for_lda(line)
            if random.random() > .99:
                print(tokens)
                text_data.append(tokens)

    dictionary = gensim.corpora.Dictionary(text_data)
    corpus = [dictionary.doc2bow(text) for text in text_data]
    pickle.dump(corpus, open('corpus.pkl', 'wb'))
    dictionary.save('dictionary.gensim')
    ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=NUM_TOPICS, id2word=dictionary, passes=15)
    ldamodel.save('model5.gensim')

    topics = ldamodel.print_topics(num_words=4)
    for topic in topics:
        print(topic)


if __name__ == "__main__":
    main()

