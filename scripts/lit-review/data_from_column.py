"""Simple script that allows a data series separated by newline characters
to be converted into a list of counts. Lines can be comma separated if more than one
value appears per line.

Used to help turn data from a column in Numbers into counts by copy-pasting.

e.g., with an input:
Cat
Dog
Cat,Dog,Moose
Moose
Dog,Cat

an output of:
[('Dog', 3), ('Cat', 3), ('Moose', 2)]

would be provided.
"""


def convert_lines_to_value_counts(data):
    """converts a string separated by newline or comma characters into a count of values
    :param str data: the string to separate
    :returns: a list of (value, count) tuples"""

    data = data.splitlines()
    data = [d.strip() for d in data if not (d.strip() == '')]
    csv_data = []
    for d in data:
        csv_data += d.split(',')
    data = csv_data

    value_counts = [(val, data.count(val)) for val in set(data)]
    value_counts = sorted(value_counts, key=lambda x: x[1], reverse=True)
    return value_counts


def main():
    data = """Cat
Doja
Cat,Doja,Moo
Moo
Doja,Cat
"""

    data = """
Web development,Embedded systems
Web development,Cloud-based infrastructure (e.g. SaaS)
Web development,Cloud-based infrastructure (e.g. SaaS)
Web development,Cloud-based infrastructure (e.g. SaaS)
Web development,Cloud-based infrastructure (e.g. SaaS)
Web development,Cloud-based infrastructure (e.g. SaaS)
Web development
Web development
Web development
Mobile development,Web development,Cloud-based infrastructure (e.g. SaaS)
Mobile development,Web development,Cloud-based infrastructure (e.g. SaaS)
Mobile development,Desktop development,Web development,Cloud-based infrastructure (e.g. SaaS)
Mobile development,Desktop development
Mobile development,Cloud-based infrastructure (e.g. SaaS)
Embedded systems
Embedded systems
Desktop development,Web development,Cloud-based infrastructure (e.g. SaaS),Embedded systems
Desktop development,Web development
Desktop development,Web development
Desktop development,Web development
Desktop development
Cloud-based infrastructure (e.g. SaaS)
Cloud-based infrastructure (e.g. SaaS)
"""
    value_counts = convert_lines_to_value_counts(data)

    [print(f"{name}: {count}") for name, count in value_counts]
    print()
    # [print(name) for name, count in value_counts]
    # print()
    # [print(count) for name, count in value_counts]


if __name__ == '__main__':
    main()
