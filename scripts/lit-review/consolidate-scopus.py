"""
This script takes the CSV file "final-pool-joined" and generates a new file
containing the key data from "final-pool-joined" and all of the other data contained in
a Scopus search results export.

It also cleans up some of the fields in "final-pool-joined".

"""

import pandas as pd

def main():
    reviewed_data = pd.read_csv('../../ms-slr-unit-ac-testing_11-21-final-pool-classified.csv')
    paper_titles = reviewed_data['Title']

    full_data = pd.read_csv('../../ms-unit-ac-testing_11-21_scopus-full-export.csv')

    unbreak_encodings(reviewed_data, full_data)

    # matches columns based on the paper titles. unbreak_encodings should fix all poorly encoded titles (if it doesn't the assertion later on will fail)
    filtered_data = full_data[(full_data["Title"].isin(paper_titles))]
    filtered_data = filtered_data.drop_duplicates(subset='Title')

    # keeps some columns from the original csv ("Title" is needed for the join operation)
    columns_to_keep = ["Title", "yes/no/maybe", "reason", "new technique", "reason (new technique column)", "paper-num",
                       "method of interest", "type(s)", "domain",
                       "unit/acceptance"]  # columns to be pulled across from old csv
    filtered_data = filtered_data.merge(reviewed_data[columns_to_keep],
                                        on="Title")

    # columns to display first in output csv
    first_columns = ['Title', 'Authors', "Year", "paper-num", "yes/no/maybe", "reason", "new technique",
                     "reason (new technique column)", "method of interest", "type(s)", "domain", "unit/acceptance"]
    filtered_data = pd.merge(filtered_data[first_columns], filtered_data)

    if len(filtered_data) != len(reviewed_data):  # a problem has occured
        print(f"expected {len(reviewed_data)}, got {len(filtered_data)}")
        print("titles without matches:")
        print(sorted(set(paper_titles).symmetric_difference(set(filtered_data['Title']))))  # shows the disparity
        raise Exception("Data set sizes not equal")
    else:
        print(len(reviewed_data), "papers in final csv")
        filtered_data.to_csv('scopus-joined.csv', index=False)


def unbreak_encodings(reviewed, full_data):
    """Some paper titles have been encoded incorrectly, affecting the merge. This manually fixes them."""
    similarities = [
        # since only small pieces of the title have encoding errors, we just string-match sections that aren't badly encoded.
        "Vulnerability mining method for industrial control network protocol based on fuzz testing",
        "QC: A property-based testing framework for L4 microkernels",
        "Calibrating Uncertainty Models for CPS Using Counterexample Validation",
        "measuring equipment for high voltage insulation modelling and simulation",
        "pin the high error regime?",
        "DBpedia FlexiFusion the Best of Wikipedia",
        "Hybrid approach based on global search algorithm for optimal placement of",
        "A synergy between logic programming and Boltzmann samplers",
        "Research on software reliability demonstration testing method based on non-parameter",
        "Sample Entropy Based Divided-period Dispatch of Net Load in New Energy Power System",
        "and Softwarization in the Design and Validation of the Stratix",
        "Study on the influence of preload on the thermal aging of transformer",
        "Tabu search algorithm for dynamic facility layout problem",
        "mobile e-commerce application prototype on Gramedia.com",
        "Validation of outsourcing teams work on agile projects of samsung",
        "Wafer yield prediction method based on improved continuous deep belief network",
        "verification of automotive SoC firmware",
        "payload measurement technique for future multi-spot-beam antennas",
        "of lexical, syntactic and semantic properties for interactive systems through model checking of formal description of dialog",
        "Virtualized hardware environments for supporting digital i",
    ]

    for similarity in similarities:
        possible_correct_titles = list(full_data.loc[full_data["Title"].str.contains(similarity), 'Title'])

        # checks that the similarity used is not ambiguous and maps title 1:1
        ambiguous_in_reviewed_papers = len([does_match for does_match in list(reviewed["Title"].str.contains(similarity)) if does_match]) != 1
        ambiguous_in_scopus_export = len(possible_correct_titles) != 1
        if ambiguous_in_reviewed_papers or ambiguous_in_scopus_export:
            raise Exception(f"similarity \"{similarity}\" is too ambiguous")

        reviewed.loc[reviewed["Title"].str.contains(similarity), 'Title'] = possible_correct_titles[0]
        print("fixed title encoding for " + list(reviewed.loc[reviewed["Title"].str.contains(similarity), 'Title'])[0])


if __name__ == '__main__':
    main()
