"""
This file contains most of the code used to generate graphs and tables
from the data file containing the MS and SLR results.
"""

import re
from collections import defaultdict

import matplotlib
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Authors,Author(s) ID,Title,Year,Source title,Volume,Issue,Art. No.,Page start,Page end,Page count,Cited by,DOI,Link,Affiliations,Authors with affiliations,Abstract,Author Keywords,Index Keywords,Molecular Sequence Numbers,Chemicals/CAS,Tradenames,Manufacturers,Funding Details,Funding Text 1,Funding Text 2,Funding Text 3,References,Correspondence Address,Editors,Sponsors,Publisher,Conference name,Conference date,Conference location,Conference code,ISSN,ISBN,CODEN,PubMed ID,Language of Original Document,Abbreviated Source Title,Document Type,Publication Stage,Open Access,Source,EID

GRAPH_DIR = "graphs"


def graph_unit_or_acceptance():
    """graphs the number of papers in the final pool based on the testing level.
    options are unit, acceptance, both or not applicable."""
    scopus = pd.read_csv('scopus-joined.csv')
    # scopus = scopus.drop(scopus[~scopus["new technique"].str.contains("y")].index)
    types = scopus['unit/acceptance']
    type_counts = types.value_counts()
    type_counts = type_counts.iloc[::-1]  # reverse the dataframe
    print(type_counts)
    type_counts.plot.barh()
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(8, 3)
    plt.ylabel("testing level")
    plt.xlabel("number of papers")
    plt.xticks(rotation=0)
    plt.tight_layout()
    plt.savefig(f"{GRAPH_DIR}/final-pool-u-or-a.png")
    plt.show()


def graph_paper_types():
    scopus = pd.read_csv('scopus-joined.csv')
    # scopus = scopus.drop(scopus[~scopus["new technique"].str.contains("y")].index)
    types_original = list(scopus['type(s)'].dropna())
    types = []
    for s in types_original:
        types += s.split('; ')
    types = pd.Series(types)
    type_counts = types.value_counts()
    print(type_counts)
    print('#########')
    print_as_latex_table(type_counts.items(), ['research approach', 'count'])
    # type_counts.plot(kind="bar")
    # plt.xlabel("paper type")
    # plt.ylabel("number of papers")
    # plt.xticks(rotation=0)
    # plt.tight_layout()
    # plt.savefig(f"{GRAPH_DIR}/final-pool-paper-types.png")
    # plt.show()


def gen_table_strategies_by_test_level():
    """generates a table of the strategies used (e.g. test-driven development) based on the
    number of papers per testing level."""
    scopus = pd.read_csv('scopus-joined.csv')
    scopus = scopus.drop(scopus[~scopus["yes/no/maybe"].str.contains("y")].index)
    scopus = scopus.drop(scopus[scopus["paper-num"].isna()].index)
    scopus = scopus.drop(scopus[scopus["method of interest"].isna()].index)

    # key is testing strategy, value is dict of testing level -> list of paper nums
    papers_per_level_per_strategy = defaultdict(lambda: defaultdict(list))
    for _, row in scopus.iterrows():
        methods = row['method of interest'].split('; ')
        testing_level = row['unit/acceptance']
        paper_num = row['paper-num']
        for method in methods:
            papers_per_level_per_strategy[method][testing_level].append(int(paper_num))
            # unit_count, acceptance_count, both_count = papers_per_level_per_strategy[method]
            # ignore "not applicable"
            # if testing_level == "unit":
            #     unit_count += 1
            # elif testing_level == "acceptance":
            #     acceptance_count += 1
            # elif testing_level == "both":
            #     both_count += 1
            # papers_per_level_per_strategy[method] = [unit_count, acceptance_count, both_count]

    table_rows = []
    for strategy, papers_per_level in papers_per_level_per_strategy.items():
        row = [strategy]
        for level in ['unit', 'acceptance', 'both']:
            paper_refs = [f"\\cite{{paper-{paper_num}}}" for paper_num in papers_per_level[level]]
            row.append(paper_refs)
        table_rows.append(row)
    table_rows.sort(key=lambda x: (len(x[1]) + len(x[2]) + len(x[3]), len(x[1])),
                    reverse=True)  # sort by total papers descending
    table_rows = [(strat, ', '.join(unit), ', '.join(acc), ', '.join(both)) for strat, unit, acc, both in table_rows]

    table_headers = ['strategy', 'unit', 'acceptance', 'both']
    print_as_latex_table(table_rows, table_headers)

    # print("\\textbf{strategy} & \\textbf{unit} & \\textbf{acceptance} & \\textbf{both} \\\\ \\hline")
    # for method, unit, acceptance, both in table_rows:
    #     # source, venue = source_title.split(':::')
    #     print(f"{method} & {unit} & {acceptance} & {both} \\\\ \\hline")

    # df.plot(x='Method of interest', y=['Unit', 'Acceptance'], kind="bar",
    #         color=['#3A9E12', '#F98800'])
    # plt.ylabel("number of papers")
    # plt.xlabel("method of interest")
    # plt.xticks(rotation=90)
    # # plt.xlabel("document type")
    # fig = matplotlib.pyplot.gcf()
    # fig.set_size_inches(9, 4)
    # # plt.tight_layout()
    # plt.gcf().subplots_adjust(bottom=0.5)
    # plt.savefig(f"{GRAPH_DIR}/final-pool-subprocess-per-method-of-interest.png")
    # plt.show()

    # print(len(scopus))
    # types_original = list(scopus['method of interest'].dropna())
    # types = []
    # for s in types_original:
    #     types += s.split('; ')
    # types = pd.Series(types)
    # type_counts = types.value_counts()
    # print(type_counts)
    # print('#########')
    # show_as_latex_table(type_counts)
    # type_counts.plot(kind="bar")
    # plt.xlabel("method of interest")
    # plt.ylabel("number of papers")
    # plt.xticks(rotation=0)
    # plt.tight_layout()
    # plt.savefig(f"{GRAPH_DIR}/final-pool-methods-of-interest.png")
    # plt.show()


def graph_years():
    """graphs total number of papers per year"""
    scopus = pd.read_csv('scopus-joined.csv')
    years = scopus['Year']
    year_counts = years.value_counts().sort_index()
    print(year_counts)
    year_counts.plot(kind="bar")
    plt.xlabel("year")
    plt.ylabel("number of papers")
    plt.xticks(rotation=0)
    plt.tight_layout()
    plt.savefig(f"{GRAPH_DIR}/secondary-pool-years.png")
    plt.show()


def graph_document_types_by_year():
    """graphs number of papers per year for each document type"""
    column_title = 'Document Type'
    scopus = pd.read_csv('scopus-joined.csv')
    # scopus = scopus.drop(scopus[~scopus["new technique"].str.contains("y")].index)
    scopus = scopus.drop(scopus[~scopus["yes/no/maybe"].str.contains("y")].index)
    scopus.loc[scopus[column_title].str.startswith("Review"), column_title] = 'Article'
    doc_types = scopus[column_title]
    doc_types = doc_types.dropna()

    scopus = scopus.loc[scopus[column_title].isin(doc_types.unique())]

    types_per_year = defaultdict(lambda: [0, 0, 0])  # book chapter, article, conference paper
    for _, row in scopus.iterrows():
        year = row['Year']
        bc_count, a_count, cp_count = types_per_year[year]
        doc_type = row[column_title]
        if doc_type == 'Conference Paper':
            cp_count += 1
        elif doc_type == 'Article':
            a_count += 1
        elif doc_type == 'Book Chapter':
            bc_count += 1
        types_per_year[year] = [bc_count, a_count, cp_count]

    temp = []
    for key, val in types_per_year.items():
        temp.append([key, *val])
    temp.sort()
    print(temp)
    df = pd.DataFrame(temp, columns=['Year', 'Book chapters', 'Articles', 'Conference papers'])

    # article_counts = doc_types.value_counts()
    # year_counts.sort_values()
    # print(article_counts)
    df.plot(x='Year', y=['Book chapters', 'Articles', 'Conference papers'], kind="bar",
            color=['#3A9E12', '#F98800', '#2D74B7'])
    plt.ylabel("number of papers")
    plt.xlabel("year")
    plt.xticks(rotation=0)
    # plt.xlabel("document type")
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(9, 4)
    plt.tight_layout()
    plt.savefig(f"{GRAPH_DIR}/secondary-pool-doc-types-per-year.png")
    plt.show()


def graph_source_titles():
    stop_words = ["Conference Proceedings -", "- Conference Proceedings", "Proceedings -", "- Proceedings",
                  " - Including ECyPS , BioEMIS , BioICT , MECO-Student Challenge",
                  r"\(including subseries Lecture Notes in Artificial Intelligence and Lecture Notes in Bioinformatics\)",
                  ": The Next Generation Information Technology Summit", ": Companion, ICSE-Companion"]
    source_title_equivs = [
        [
            "Proceedings - IEEE International Conference on Software Testing, Verification and Validation Workshops, ICSTW",
            "IEEE International Conference on Software Testing, Verification and Validation Workshops, ICSTW - Proceedings",
        ]
    ]
    venue_equivs = [
        [
            "International Conference on Software and Systems Process, ICSSP",
            "International Conference on Software and System Process, ICSSP",
        ],
        [
            "International Conference on Tests and Proofs, TAP",
            "International Conference on Tests and Proofs, TAP , Held as Part of the Software Technologies: Applications and Foundations, STAF",
            "International Conference on Tests and Proofs, TAP Held as Part of STAF",
            "International Conference on Tests and Proofs, TAP , held as part of the World Congress on Formal Methods, FM",
        ],
        [
            "Brazilian Symposium on Systematic and Automated Software Testing, CBSOFT",
            "Brazilian Symposium on Systematic and Automated Software Testing, SAST , co-located with the Brazilian Conference on Software: Theory and Practice, CBSOFT",
        ],
    ]
    venue_stop_words = ["IFIP TC 5, TC 12, WG 8.4, WG 8.9, WG 12.9"]

    conference_needed = {
        'Lecture Notes in Computer Science', 'ACM International Conference Proceeding Series',
        'CEUR Workshop Proceedings', 'Lecture Notes in Business Information Processing',
        # 'International Conference on Software Engineering',
    }
    scopus = pd.read_csv('scopus-joined.csv')
    scopus = scopus.drop(scopus[~scopus["yes/no/maybe"].str.contains("y")].index)
    source_title_col = 'Source title'
    scopus = scopus.dropna(subset=[source_title_col])
    # scopus['Source and venue'] = scopus['Source title'].str.cat(scopus['Conference name'], sep=" ")
    # source_titles = scopus['Source and venue'].dropna()
    # source_titles = scopus['Source title']
    # source_titles = source_titles.dropna()

    clean_source_names(scopus, source_title_col, source_title_equivs, stop_words)
    clean_source_names(scopus, 'Conference name', venue_equivs, venue_stop_words)

    sset = set()
    for i, row in scopus.iterrows():
        source_and_venue = row[source_title_col]
        # if source_and_venue in conference_needed:
        if source_and_venue in conference_needed:
            source_and_venue += f" (conference: {row['Conference name']})"
            sset.add(row['Conference name'])
        # else:
        #     source_and_venue += ":::"
        scopus.at[i, 'Source and venue'] = source_and_venue
    [print(s) for s in sorted(sset)]
    print()

    source_counts = scopus['Source and venue'].value_counts()
    print(f"num sources: {len(source_counts)}")
    source_counts = source_counts[source_counts > 2]
    print_as_latex_table(source_counts.items(), ['Source title (and conference, if applicable)', 'Count'])

    # fig = plt.figure()  # plt.figure(figsize=(8, 8), dpi=300)
    # fig.subplots_adjust(bottom=0.4)
    # print(f"{source_title}")
    # source_counts.plot(kind="bar")
    # plt.tight_layout()
    # plt.show()


def print_as_latex_table(rows, headers=None):
    if headers is not None:
        title_row = " & ".join([f"\\textbf{{{header}}}" for header in headers]) + " \\\\ \\hline"
        print(title_row)
    for row in rows:
        latex = " & ".join([f"{val}" for val in row]) + " \\\\ \\hline"
        print(latex)


def clean_source_names(scopus, column_name, equivalences, stop_words):
    """
    :param equivalences: iterable of values in the column that should be considered the same (1st item is used for all)
    :param scopus: DataFrame of scopus data
    :param column_name: name of column to clean
    :param stop_words: iterable of substrings that should be removed from the column
    :return:
    """
    scopus[column_name] = scopus[column_name].replace(np.nan, '', regex=True)
    scopus[column_name] = scopus[column_name].map(
        lambda s: re.sub(r'20\d\d', '', s)
    ).map(
        lambda s: re.sub(r'\d+(st|nd|rd|th)', '', s)
    )
    for word in stop_words:
        scopus[column_name] = scopus[column_name].map(lambda s: re.sub(word, '', s))
    scopus[column_name] = scopus[column_name].map(
        lambda s: re.sub(r'\s+', ' ', s).strip()
    )
    for eq_class in equivalences:
        value = eq_class[0]
        for title in eq_class[1:]:
            scopus[column_name].replace(title, value, inplace=True)


def print_survey_question_tables():
    # question types
    closed = "closed"
    openn = "open"
    hybrid = "hybrid"

    # question formats
    freeform = "free-form"
    category = nominal = "nominal (single)"
    multichoice = "nominal (multiple)"
    likert = ordinal = "ordinal"
    interval = "interval"

    survey_demographic_questions = [
        ["How many years have you been working in the software industry?", closed, interval],
        ["What is your primary role in your current job?", hybrid, category],
        ["What is the size of the team you currently work with day-to-day as part of your current project/product?",
         closed, interval],
        ["What approach to software development do you take as part of your current project/product?", closed,
         category],
        ["How much professional experience do you have working with automated testing?", closed, interval],
        ["In your current company / for your current client, what types of projects do you primarily work on?", closed,
         multichoice],
        ["What application domain is you current project targeted at?", hybrid, category],
        ["In which country is your current employer primarily based?", closed, category],
    ]

    tdd_questions = [
        [
            "How familiar are you with the concept of unit test-driven development, acceptance test-driven development or other test-first strategies used in relation to unit testing?",
            closed, likert],
        ["What level of training have you received for test-first strategies?", hybrid, multichoice],
        ["How often do you use test-first strategies in practice?", closed, likert],
        ["Is the use of this strategy a personal choice or is it a team-wide practice?", closed, category],
        ["Do you use this strategy for unit or acceptance testing?", hybrid, multichoice],
        ["How effective do you find this strategy for unit testing?", closed, likert],
        ["Please provide a reason(s) as to why you find this strategy to be / not to be effective for unit testing.",
         openn, freeform],
        ["How effective do you find this strategy for acceptance testing?", closed, likert],
        [
            "Please provide a reason(s) as to why you find this strategy to be / not to be effective for acceptance testing.",
            openn, freeform],
    ]

    mbtg_questions = [
        ["How familiar are you with the concept of a testing strategy that uses models to generate tests?", closed,
         likert],
        ["What level of training have you received for model-based testing strategies?", hybrid, multichoice],
        ["How often do you use model-based testing strategies in practice?", closed, likert],
        ["Is the use of this strategy a personal choice or is it a team-wide practice?", closed, category],
        ["Do you use this strategy for unit or acceptance testing?", hybrid, multichoice],
        ["How effective do you find this strategy for unit testing?", closed, likert],
        ["Please provide a reason(s) as to why you find this strategy to be / not to be effective for unit testing.",
         openn, freeform],
        ["How effective do you find this strategy for acceptance testing?", closed, likert],
        [
            "Please provide a reason(s) as to why you find this strategy to be / not to be effective for acceptance testing.",
            openn, freeform]
    ]

    question_number = 1
    for questions in [survey_demographic_questions, tdd_questions, mbtg_questions]:
        for i, (question, q_type, q_format) in enumerate(questions):
            questions[i][0] = f"Q{question_number}. {question}"
            assert q_type in [closed, openn, hybrid]
            assert q_format in [category, ordinal, multichoice, interval, freeform]
            question_number += 1

    table_headers = ["question", "type", "format"]
    print()
    # print_as_latex_table(survey_demographic_questions, table_headers)
    # print_as_latex_table(tdd_questions, table_headers)
    print_as_latex_table(mbtg_questions, table_headers)


def print_survey_encoding_tables():
    agreeing = "agree"
    disagree = "disagree"
    no_commt = "no comment"
    unclearr = "unclear"
    utdd_encoding = [
        ['1', no_commt, no_commt],
        ['2', no_commt, no_commt],
        ['3', no_commt, no_commt],
        ['4', no_commt, agreeing],
        ['5', no_commt, no_commt],
        ['6', agreeing, no_commt],
        ['7', agreeing, no_commt],
        ['8', agreeing, no_commt],
        ['9', agreeing, no_commt],
        ['10', no_commt, no_commt],
        ['11', agreeing, no_commt],
        ['12', agreeing, no_commt],
        ['13', no_commt, agreeing],
        ['14', no_commt, no_commt]
    ]
    atdd_encoding = [
        ['1', unclearr, unclearr],  # as it helps to test the system from the user point of view
        ['2', no_commt, no_commt],  # Difficulty of mocking external services
        ['3', unclearr, unclearr],  # Effective but not covering enough
        ['4', no_commt, no_commt],  # Finding bugs as soon as possible with either simulated or real data from the customer.
        ['5', no_commt, no_commt],  # Help refactor
        ['6', no_commt, no_commt],  # only useful when you're nearing the end of the development. which means that possibly the scope has evolved and the test has to be maintained throughout. The larger the scope of the test, the more likely it is to require changes between the start and the end of the development.
    ]
    for encoding in [utdd_encoding, atdd_encoding]:
        counts = defaultdict(lambda: defaultdict(int))  # premise -> agree/disagree/no_c/unclear -> count
        for _, premise_1, premise_2 in encoding:
            counts['premise 1'][premise_1] += 1
            counts['premise 2'][premise_2] += 1
        table = []
        headers = ['', 'premise 1', 'premise 2']
        for key in [agreeing, disagree, no_commt, unclearr]:
            table.append([key, counts['premise 1'][key], counts['premise 2'][key]])
        print()
        print()
        print_as_latex_table(table, headers)


if __name__ == '__main__':
    font = {'family': 'normal',
            'size': 13}
    matplotlib.rc('font', **font)

    #graph_unit_or_acceptance()
    graph_paper_types()
    gen_table_strategies_by_test_level()
    #graph_years()
    graph_document_types_by_year()
    graph_source_titles()
    #print_survey_question_tables()
    #print_survey_encoding_tables()
    
