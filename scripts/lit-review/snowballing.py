import pandas as pd

# Authors,Author(s) ID,Title,Year,Source title,Volume,Issue,Art. No.,Page start,Page end,Page count,Cited by,DOI,Link,Affiliations,Authors with affiliations,Abstract,Author Keywords,Index Keywords,Molecular Sequence Numbers,Chemicals/CAS,Tradenames,Manufacturers,Funding Details,Funding Text 1,Funding Text 2,Funding Text 3,References,Correspondence Address,Editors,Sponsors,Publisher,Conference name,Conference date,Conference location,Conference code,ISSN,ISBN,CODEN,PubMed ID,Language of Original Document,Abbreviated Source Title,Document Type,Publication Stage,Open Access,Source,EID


def main():
    scopus = pd.read_csv('scopus-joined.csv')
    references = scopus['References']
    for ref in references.iloc[0].split(';'):
        print(ref)

    # full_data = pd.read_csv('../../scopus-full.csv')
    # filtered_data = full_data[(full_data["Title"].isin(paper_titles))]
    # filtered_data = filtered_data.drop_duplicates(subset='Title')
    #
    # assert len(filtered_data) == len(reviewed)
    # filtered_data.to_csv('scopus-joined.csv', index=False)


if __name__ == '__main__':
    main()
