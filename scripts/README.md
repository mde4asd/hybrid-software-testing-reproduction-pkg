# Hybrid Software Testing scripts

This folder contains all the scripts used to generate graphs from the data in `final-pool-joined.csv`.

DISCLAIMER: 

* lots of hardcoding. 
* python scripts, needs `pandas`, `matplotlib`, `numpy` and `nltk`

The main workflow required to update the graphs is as follows:

- The `consolidate-scopus.py` file is used to merge the `ms-slr-unit-ac-testing_11-21-final-pool-classified` with the `ms-unit-ac-testing_11-21_scopus-full-export.csv` file, creating the file `scopus-joined.csv`. All graphs are generated using this file.
  - `scopus-joined.csv` is included already; you only need to update it if you change `final-pool-joined.csv`
- You can then use the scripts `gen-graphs.py` and `graph-article-keywords.py` to generate graphs and table of your choice.

Documentation is provided for each python file explaining the purpose of the file and the functions included.
The Pandas library is used for most of the data manipulation and graphing (see https://pandas.pydata.org/).

A `venv` folder is included with a virtual environment containing all packages required. However, you can use your own environment and install any packages required by each individual script if this does not work for you.

A number of models are also included; these are used by `graph-article-keywords.py` for keyword stemming.
