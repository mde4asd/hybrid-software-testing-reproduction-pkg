# Reproduction package for the paper "Overlap between Automated Unit and Acceptance Testing – a Systematic Literature Review"

In this repository, you will find:

* some `scripts` used to extract data and generate the graphs and some tables in the paper
* a `csv` file with all the annotated data extracted from scopus, i.e. `ms-unit-ac-testing_11-21_scopus-full-export` (note that the export is for the final version and contains 2 more years, i.e. 2011/12 compared to preprint version).
* a `csv` file with the paper selected during the mapping study and additional annotation / classification, i.e. `final-pool-joined`.
* authors notes on papers, i.e. `EASE_2022_TESTING_SLR_paper-notes.pdf`
* the preprint of the EASE 2022 paper, i.e. `EASE_2022_TESTING_SLR_jvb-fgi-mga_preprint.pdf` (preprint contains the SLR for paper from 2013 included, final version is over 10 years, so from 2011 included).
* additional survey conducted as a follow-up of the MS-SLR (not published, both the questions and responses)

The two `csv` files are also available on [Zenodo](https://doi.org/10.5281/zenodo.6400077).

A link to [EASE'22 presentation slides](https://drive.google.com/file/d/1QwnBbWXBoJyh8HdMxE1l-g4oigd8XqcH/view?usp=sharing).
